//
//  BluetoothMessages.swift
//  BluetoothMessages
//

import UIKit
import Foundation
import CoreBluetooth

public protocol BMBluetoothAgentDelegate {
    func updateAgent(agent: BMBluetoothAgent, status: BMAgentStatus)
    func didConnectedTo(agent: BMBluetoothAgent)
}

public protocol BMBluetoothConectionDelegate {
    func didConnectTo(agent: BMBluetoothAgent)
    func didDisconnectTo(agent: BMBluetoothAgent)
    func didReceiveData(data: Data?, error: Error?)
    func didWriteData(error: Error?)
}

public enum BMAgentStatus : String {
    case connected = "Connected"
    case disConnected = "Disconnected"
    case new = "Discovered"
    case updateName = "Updated"
}

public struct BMMessage {
    public var id: Int!
    public var content : Data!
    public var readed : Bool!
    
    public var toString : String {
        get {
            return String(decoding: content, as: UTF8.self)
        }
    }
    
    public init(){}
}

public struct BMBluetoothAgent {
    public var description : String!
    public var messages : [BMMessage] = [BMMessage]()
    public var status : BMAgentStatus!
    public var RSSI : NSNumber! = 12345
    public var uuid : String! = ""
}

public enum ErrorResult {
    case success(Data)
    case failure(Error)
}


@objc(BluetoothAgentManager)
public class BluetoothAgentManager : NSObject {
    @objc
    public static let SERVICE_UUID = CBUUID(string: "9f37e282-60b6-42b1-a02f-7341da5e2eba")
    @objc
    public static let CHARACTERISTIC_UUID = CBUUID(string: "4DF91029-B356-463E-9F48-BAB077BF3EF5")

    private var BMDelegate : BMBluetoothAgentDelegate!
    public  var BMConnectionDelegate : BMBluetoothConectionDelegate?
    
    private var centralManager : CBCentralManager!
    private var peripheralManager : CBPeripheralManager!
    
    private var deviceUUID : String!
    private var deviceName : String!
    
    private var characteristic : CBCharacteristic!
    
    private var discoveredAgents : [CBPeripheral] = [CBPeripheral]()
    private var _controlRetrievedUUIDS = [String]()
    
    private var agents : [BMBluetoothAgent] = [BMBluetoothAgent]()
    private var agent = BMBluetoothAgent()
    
    private var _retrieveAgents: Timer?
    private var _scanningNewAgents: Timer?
    
    public override init() {}
    
    public convenience init(delegate: BMBluetoothAgentDelegate) {
        self.init()
        
        BMDelegate = delegate
        
        deviceUUID = UIDevice.current.identifierForVendor?.uuidString
        deviceName = UIDevice.current.name
        
        centralManager = CBCentralManager(delegate: self, queue: nil)
        peripheralManager = CBPeripheralManager(delegate: self, queue: nil)
        
        _retrieveAgents = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(updateAgents), userInfo: nil, repeats: true)
        _scanningNewAgents = Timer.scheduledTimer(timeInterval: 20, target: self, selector: #selector(redoScan), userInfo: nil, repeats: true)
    }
    
    @objc
    private func updateAgents(){
        let uuids = discoveredAgents.map({UUID(uuid: $0.identifier.uuid)})           // lista de uuids para recuperar
        let peripherals = centralManager.retrievePeripherals(withIdentifiers: uuids) // lista de perifericos recuperados
        let peripherasl_uuids = peripherals.map({$0.identifier.uuidString})          // uuidsString deles
        
        let not_retrieved = agents.filter{ !peripherasl_uuids.contains($0.uuid) }    // agentes nao recuperados
        
        if not_retrieved.count > 0 {
            var indexes = [Int]()
            
            for ag in not_retrieved {
                let i = agents.firstIndex(where: { ag.uuid == $0.uuid })!
                indexes.append(i)
            }
            
            indexes.forEach({ agents.remove(at: $0) })
        }

    }
    
    @objc
    private func redoScan() { // sometimes centralManager dont discover new ones, so its necessary to scan again
        centralManager.scanForPeripherals(withServices: [BluetoothAgentManager.SERVICE_UUID], options: nil)
    }
    
    public func returnAllAgents() -> [BMBluetoothAgent] {
        return agents
    }
    
    public func BMConectToAgent(agent: BMBluetoothAgent){
        if let peripheral = discoveredAgents.first(where: {$0.identifier.uuidString == agent.uuid}) {
            centralManager.connect(peripheral, options: nil)
        }

    }
    
    public func BMAgentReadyToSendMessage(agent: BMBluetoothAgent, completion : @escaping () -> ()) {
        print("BluetoothMessagesDelegate:  BMAgentReadyToSendMessage")
        
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (timer) in
            
            if self.agent.uuid == agent.uuid {
                timer.invalidate()
                completion()
            }
        }
        
    }
    
    public func BMSendMessageTo(_ agent: BMBluetoothAgent,_ message: BMMessage, handler : ((_ success : Bool) -> ())? = nil) {
        guard let peripheral = discoveredAgents.first(where: {$0.identifier.uuidString == agent.uuid}) else {
            print("ERRO AO ENVIAR MENSAGEM: agente não conectado")
            handler?(false)
            return
        }
        peripheral.writeValue(message.content, for:characteristic, type: .withResponse)
    }
}


// ********     as central     ******** //
extension BluetoothAgentManager : CBCentralManagerDelegate {

    public func centralManagerDidUpdateState(_ central: CBCentralManager) {
        print("BluetoothMessages:  centralManagerDidUpdateState")
        switch central.state {
        case .unknown:
            print("BluetoothMessages: unknown")
        case .resetting:
            print("BluetoothMessages: resetting")
        case .unsupported:
            print("BluetoothMessages: unsupported")
        case .unauthorized:
            print("BluetoothMessages: unauthorized")
        case .poweredOff:
            print("BluetoothMessages: poweredOff")
            centralManager?.stopScan()
        case .poweredOn:
            print("BluetoothMessages: poweredOn")
            centralManager.scanForPeripherals(withServices: [BluetoothAgentManager.SERVICE_UUID], options: nil)
        @unknown default:
            break
        }
    }
    
    

    public func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        print("BluetoothMessages:  didDiscover peripheral")
        if !discoveredAgents.contains(peripheral) {
            
            var _agent = BMBluetoothAgent()
            
            _agent.description = peripheral.name ?? peripheral.identifier.description
            _agent.uuid = peripheral.identifier.uuidString
            _agent.status = .new
            _agent.RSSI = RSSI
            
            agents.append(_agent)
            
            discoveredAgents.append(peripheral)
            BMDelegate.updateAgent(agent: _agent, status: .new)
        } else {
            let i = agents.firstIndex(where: {$0.uuid == peripheral.identifier.uuidString})!
            agents[i].status = .disConnected
            agents[i].RSSI = RSSI
        }
    }

    public func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("BluetoothMessages:  didFailToConnect")
            agent.status = .disConnected
            BMDelegate.updateAgent(agent: agent, status: .disConnected)
    }

    public func centralManager(_ centralManager: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("BluetoothMessages:  didConnect")
        
        agent = agents.first(where: {$0.uuid == peripheral.identifier.uuidString})!
        
        agent.status = .connected
        BMDelegate.updateAgent(agent: agent, status: .connected)
    
        peripheral.delegate = self
        peripheral.discoverServices([BluetoothAgentManager.SERVICE_UUID])
        BMDelegate.updateAgent(agent: agent, status: .connected)
        BMConnectionDelegate?.didConnectTo(agent: agent)
        
    }

    public func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print("BluetoothMessages:  didDisconnectPeripheral")
        
        if agent.uuid ==  peripheral.identifier.uuidString {
            agent.status = .disConnected
            BMDelegate.updateAgent(agent: agent, status: .disConnected)
            BMConnectionDelegate?.didDisconnectTo(agent: agent)
        }
    }
    
    

}
// ********     as peripheral     ******** //
extension BluetoothAgentManager : CBPeripheralManagerDelegate, CBPeripheralDelegate {
    
    public func centralManager(_ central: CBCentralManager, connectionEventDidOccur event: CBConnectionEvent, for peripheral: CBPeripheral) {
        
    }
    public func peripheralManager(_ peripheral: CBPeripheralManager, central: CBCentral, didSubscribeTo characteristic: CBCharacteristic) {
        print("BluetoothMessages:  didSubscribeTo")
        if let per = discoveredAgents.first(where: {$0.identifier.uuidString == central.identifier.uuidString}),
           let _ag = agents.first(where: {$0.uuid == per.identifier.uuidString}) {
            BMDelegate.didConnectedTo(agent: _ag)
        }

    }
    
    public func peripheralDidUpdateName(_ peripheral: CBPeripheral) {
        print("BluetoothMessages:  peripheralDidUpdateName")
        if let a = agents.firstIndex(where: {$0.uuid == peripheral.identifier.uuidString}) {
            agents[a].description = peripheral.name ?? peripheral.identifier.description
            BMDelegate.updateAgent(agent: agents[a], status: .updateName)
            return
        }
    }

    public func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        print("BluetoothMessages:  peripheralManagerDidUpdateState")
        guard peripheral.state == .poweredOn else { return }

        let characteristic = CBMutableCharacteristic(type: BluetoothAgentManager.CHARACTERISTIC_UUID,
                                                     properties: [.write, .notify, .read],
                                                     value: nil,
                                                     permissions: [ .writeable,.readable])

        let service = CBMutableService(type: BluetoothAgentManager.SERVICE_UUID, primary: true)
        service.characteristics = [characteristic]

        peripheralManager.add(service)
    }
    
    public func peripheralManager(_ peripheral: CBPeripheralManager, didAdd service: CBService, error: Error?) {
        print("BluetoothMessages:  didAdd service")
        if (error != nil) {
            print("BluetoothMessages: PerformerUtility.publishServices() returned error: \(error!.localizedDescription)")
            print("BluetoothMessages: Providing the reason for failure: \(error!.localizedDescription)")
        }
        else {
            peripheralManager.startAdvertising( [CBAdvertisementDataServiceUUIDsKey: [service.uuid], CBAdvertisementDataLocalNameKey: deviceName ?? "undefined"])
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didModifyServices invalidatedServices: [CBService]) {
        print("BluetoothMessages: didModifyServices")
        
        if invalidatedServices.first(where: {$0.uuid == BluetoothAgentManager.SERVICE_UUID }) != nil {
            agent.status = .disConnected
            BMDelegate.updateAgent(agent: agent, status: .disConnected)
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        print("BluetoothMessages: didDiscoverServices")

        if let error = error {
            print("BluetoothMessages: Unable to discover services: \(error.localizedDescription)")
            return
        }
        
        peripheral.services?.forEach { service in
            peripheral.discoverCharacteristics([BluetoothAgentManager.CHARACTERISTIC_UUID], for: service)
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        print("BluetoothMessages: didDiscoverCharacteristicsFor")
        if let error = error {
            print("BluetoothMessages: Unable to discover characteristics: \(error.localizedDescription)")
            return
        }
        
        service.characteristics?.forEach { characteristic in
            guard characteristic.uuid == BluetoothAgentManager.CHARACTERISTIC_UUID else { return }
            
            peripheral.setNotifyValue(true, for: characteristic)
            
            self.characteristic = characteristic
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
        print("BluetoothMessages: didUpdateNotificationStateFor")
        if let error = error {
            print("BluetoothMessages: Characteristic update notification error: \(error.localizedDescription)")
            return
        }
        
        guard characteristic.uuid == BluetoothAgentManager.CHARACTERISTIC_UUID else { return }
        
        if characteristic.isNotifying {
            print("BluetoothMessages: Characteristic notifications have begun.")
        } else {
            print("BluetoothMessages: Characteristic notifications have stopped. Disconnecting.")
            centralManager.cancelPeripheralConnection(peripheral)
        }
    }
    
    public func peripheralIsReady(toSendWriteWithoutResponse peripheral: CBPeripheral) {
        print("BluetoothMessages: peripheralIsReady")
    }

    public func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        print("BluetoothMessages: didWriteValueFor")
        
        if let error = error {
            print("BluetoothMessages: Characteristic value update failed: \(error.localizedDescription)")
            BMConnectionDelegate?.didWriteData(error: error)
            return
        }
        
        BMConnectionDelegate?.didWriteData(error: nil)
    }

    public func peripheralManager(_ peripheral: CBPeripheralManager, didReceiveRead request: CBATTRequest) {
        print("BluetoothMessages:  didReceiveRead")
        guard let data = request.value else { return }
        let message = String(decoding: data, as: UTF8.self)
        
        peripheral.respond(to: request, withResult: .success)
        print(message)
    }

    public func peripheralManager(_ peripheral: CBPeripheralManager, didReceiveWrite requests: [CBATTRequest]) {
        print("BluetoothMessages:  didReceiveWrite")
        guard let request = requests.first, let data = request.value else { return }
        let message = String(decoding: data, as: UTF8.self)
        
        peripheral.respond(to: request, withResult: .success)
        print(message)
        BMConnectionDelegate?.didReceiveData(data: data, error: nil)
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        print("BluetoothMessages:  didUpdateValueFor")
        if let error = error {
            print("BluetoothMessages: Characteristic value update failed: \(error.localizedDescription)")
            return
        }

        guard let data = characteristic.value else { return }

        let message = String(decoding: data, as: UTF8.self)
        print("didReceiveData: \(message)")
        BMConnectionDelegate?.didReceiveData(data: data, error: nil)
    }
}
